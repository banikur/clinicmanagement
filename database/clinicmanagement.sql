-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 27 Apr 2024 pada 05.49
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinicmanagement`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `autentikasi`
--

CREATE TABLE `autentikasi` (
  `ID_Pengguna` int(11) NOT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Nama_Lengkap` varchar(100) DEFAULT NULL,
  `Role` enum('Admin','Farmasi','Dokter') DEFAULT NULL,
  `kode_pengguna` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `autentikasi`
--

INSERT INTO `autentikasi` (`ID_Pengguna`, `Username`, `Password`, `Nama_Lengkap`, `Role`, `kode_pengguna`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500', 'admin', 'Admin', 'US0001'),
(2, 'farmasi', '0192023a7bbd73250516f069df18b500', 'farmasi', 'Farmasi', 'US0002'),
(3, 'dokter', '0192023a7bbd73250516f069df18b500', 'dokter 1', 'Dokter', 'US0003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_dokter`
--

CREATE TABLE `jadwal_dokter` (
  `ID_Jadwal` int(11) NOT NULL,
  `ID_Pegawai` int(11) DEFAULT NULL,
  `Hari` varchar(10) DEFAULT NULL,
  `Jam_Mulai` time DEFAULT NULL,
  `Jam_Selesai` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mst_obat`
--

CREATE TABLE `mst_obat` (
  `ID_Obat` int(11) NOT NULL,
  `Nama_Obat` varchar(100) DEFAULT NULL,
  `Deskripsi` text DEFAULT NULL,
  `Harga` decimal(10,2) DEFAULT NULL,
  `Stok` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mst_pasien`
--

CREATE TABLE `mst_pasien` (
  `ID_Pasien` int(11) NOT NULL,
  `Nama` varchar(100) DEFAULT NULL,
  `Alamat` varchar(255) DEFAULT NULL,
  `Nomor_Telepon` varchar(20) DEFAULT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Jenis_Kelamin` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mst_pegawai`
--

CREATE TABLE `mst_pegawai` (
  `ID_Pegawai` int(11) NOT NULL,
  `Nama` varchar(100) DEFAULT NULL,
  `Alamat` varchar(255) DEFAULT NULL,
  `Nomor_Telepon` varchar(20) DEFAULT NULL,
  `Jabatan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemeriksaan_medis`
--

CREATE TABLE `pemeriksaan_medis` (
  `ID_Pemeriksaan` int(11) NOT NULL,
  `ID_Pasien` int(11) DEFAULT NULL,
  `ID_Pegawai` int(11) DEFAULT NULL,
  `Tanggal_Pemeriksaan` date DEFAULT NULL,
  `Diagnosis` text DEFAULT NULL,
  `ID_Obat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftaran_pasien`
--

CREATE TABLE `pendaftaran_pasien` (
  `ID_Pendaftaran` int(11) NOT NULL,
  `ID_Pasien` int(11) DEFAULT NULL,
  `Tanggal_Pendaftaran` date DEFAULT NULL,
  `ID_Pegawai` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekam_medis`
--

CREATE TABLE `rekam_medis` (
  `ID_Rekam_Medis` int(11) NOT NULL,
  `ID_Pasien` int(11) DEFAULT NULL,
  `ID_Pegawai` int(11) DEFAULT NULL,
  `Tanggal` date DEFAULT NULL,
  `Catatan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `report_jumlah_pemeriksaan_tiap_dokter`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `report_jumlah_pemeriksaan_tiap_dokter` (
`Nama_Dokter` varchar(100)
,`Jumlah_Pemeriksaan` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `report_jumlah_pendaftaran_pasien_bulanan`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `report_jumlah_pendaftaran_pasien_bulanan` (
`Bulan` int(2)
,`Jumlah_Pendaftaran` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `report_kunjungan_rekam_medis_pasien`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `report_kunjungan_rekam_medis_pasien` (
`ID_Pasien` int(11)
,`Nama_Pasien` varchar(100)
,`Jumlah_Pemeriksaan` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `report_stock_obat`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `report_stock_obat` (
`Nama_Obat` varchar(100)
,`Stok` int(11)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `resep_obat`
--

CREATE TABLE `resep_obat` (
  `ID_Resep` int(11) NOT NULL,
  `ID_Pemeriksaan` int(11) DEFAULT NULL,
  `ID_Obat` int(11) DEFAULT NULL,
  `Jumlah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur untuk view `report_jumlah_pemeriksaan_tiap_dokter`
--
DROP TABLE IF EXISTS `report_jumlah_pemeriksaan_tiap_dokter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_jumlah_pemeriksaan_tiap_dokter`  AS SELECT `p`.`Nama` AS `Nama_Dokter`, count(0) AS `Jumlah_Pemeriksaan` FROM (`pemeriksaan_medis` `pm` join `mst_pegawai` `p` on(`pm`.`ID_Pegawai` = `p`.`ID_Pegawai`)) GROUP BY `pm`.`ID_Pegawai``ID_Pegawai`  ;

-- --------------------------------------------------------

--
-- Struktur untuk view `report_jumlah_pendaftaran_pasien_bulanan`
--
DROP TABLE IF EXISTS `report_jumlah_pendaftaran_pasien_bulanan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_jumlah_pendaftaran_pasien_bulanan`  AS SELECT month(`pendaftaran_pasien`.`Tanggal_Pendaftaran`) AS `Bulan`, count(0) AS `Jumlah_Pendaftaran` FROM `pendaftaran_pasien` GROUP BY month(`pendaftaran_pasien`.`Tanggal_Pendaftaran`)  ;

-- --------------------------------------------------------

--
-- Struktur untuk view `report_kunjungan_rekam_medis_pasien`
--
DROP TABLE IF EXISTS `report_kunjungan_rekam_medis_pasien`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_kunjungan_rekam_medis_pasien`  AS SELECT `pm`.`ID_Pasien` AS `ID_Pasien`, `p`.`Nama` AS `Nama_Pasien`, count(0) AS `Jumlah_Pemeriksaan` FROM (`pemeriksaan_medis` `pm` join `mst_pasien` `p` on(`pm`.`ID_Pasien` = `p`.`ID_Pasien`)) GROUP BY `pm`.`ID_Pasien`, `p`.`Nama``Nama`  ;

-- --------------------------------------------------------

--
-- Struktur untuk view `report_stock_obat`
--
DROP TABLE IF EXISTS `report_stock_obat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_stock_obat`  AS SELECT `mst_obat`.`Nama_Obat` AS `Nama_Obat`, `mst_obat`.`Stok` AS `Stok` FROM `mst_obat` ORDER BY `mst_obat`.`Stok` AS `DESCdesc` ASC  ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `autentikasi`
--
ALTER TABLE `autentikasi`
  ADD PRIMARY KEY (`ID_Pengguna`) USING BTREE;

--
-- Indeks untuk tabel `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD PRIMARY KEY (`ID_Jadwal`) USING BTREE,
  ADD KEY `ID_Pegawai` (`ID_Pegawai`) USING BTREE;

--
-- Indeks untuk tabel `mst_obat`
--
ALTER TABLE `mst_obat`
  ADD PRIMARY KEY (`ID_Obat`) USING BTREE;

--
-- Indeks untuk tabel `mst_pasien`
--
ALTER TABLE `mst_pasien`
  ADD PRIMARY KEY (`ID_Pasien`) USING BTREE;

--
-- Indeks untuk tabel `mst_pegawai`
--
ALTER TABLE `mst_pegawai`
  ADD PRIMARY KEY (`ID_Pegawai`) USING BTREE;

--
-- Indeks untuk tabel `pemeriksaan_medis`
--
ALTER TABLE `pemeriksaan_medis`
  ADD PRIMARY KEY (`ID_Pemeriksaan`) USING BTREE,
  ADD KEY `ID_Pasien` (`ID_Pasien`) USING BTREE,
  ADD KEY `ID_Pegawai` (`ID_Pegawai`) USING BTREE,
  ADD KEY `ID_Obat` (`ID_Obat`) USING BTREE;

--
-- Indeks untuk tabel `pendaftaran_pasien`
--
ALTER TABLE `pendaftaran_pasien`
  ADD PRIMARY KEY (`ID_Pendaftaran`) USING BTREE,
  ADD KEY `ID_Pasien` (`ID_Pasien`) USING BTREE,
  ADD KEY `ID_Pegawai` (`ID_Pegawai`) USING BTREE;

--
-- Indeks untuk tabel `rekam_medis`
--
ALTER TABLE `rekam_medis`
  ADD PRIMARY KEY (`ID_Rekam_Medis`) USING BTREE,
  ADD KEY `ID_Pasien` (`ID_Pasien`) USING BTREE,
  ADD KEY `ID_Pegawai` (`ID_Pegawai`) USING BTREE;

--
-- Indeks untuk tabel `resep_obat`
--
ALTER TABLE `resep_obat`
  ADD PRIMARY KEY (`ID_Resep`) USING BTREE,
  ADD KEY `ID_Pemeriksaan` (`ID_Pemeriksaan`) USING BTREE,
  ADD KEY `ID_Obat` (`ID_Obat`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `autentikasi`
--
ALTER TABLE `autentikasi`
  MODIFY `ID_Pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `mst_obat`
--
ALTER TABLE `mst_obat`
  MODIFY `ID_Obat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `mst_pasien`
--
ALTER TABLE `mst_pasien`
  MODIFY `ID_Pasien` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD CONSTRAINT `jadwal_dokter_ibfk_1` FOREIGN KEY (`ID_Pegawai`) REFERENCES `mst_pegawai` (`ID_Pegawai`);

--
-- Ketidakleluasaan untuk tabel `pemeriksaan_medis`
--
ALTER TABLE `pemeriksaan_medis`
  ADD CONSTRAINT `pemeriksaan_medis_ibfk_1` FOREIGN KEY (`ID_Pasien`) REFERENCES `mst_pasien` (`ID_Pasien`),
  ADD CONSTRAINT `pemeriksaan_medis_ibfk_2` FOREIGN KEY (`ID_Pegawai`) REFERENCES `mst_pegawai` (`ID_Pegawai`),
  ADD CONSTRAINT `pemeriksaan_medis_ibfk_3` FOREIGN KEY (`ID_Obat`) REFERENCES `mst_obat` (`ID_Obat`);

--
-- Ketidakleluasaan untuk tabel `pendaftaran_pasien`
--
ALTER TABLE `pendaftaran_pasien`
  ADD CONSTRAINT `pendaftaran_pasien_ibfk_1` FOREIGN KEY (`ID_Pasien`) REFERENCES `mst_pasien` (`ID_Pasien`),
  ADD CONSTRAINT `pendaftaran_pasien_ibfk_2` FOREIGN KEY (`ID_Pegawai`) REFERENCES `mst_pegawai` (`ID_Pegawai`);

--
-- Ketidakleluasaan untuk tabel `rekam_medis`
--
ALTER TABLE `rekam_medis`
  ADD CONSTRAINT `rekam_medis_ibfk_1` FOREIGN KEY (`ID_Pasien`) REFERENCES `mst_pasien` (`ID_Pasien`),
  ADD CONSTRAINT `rekam_medis_ibfk_2` FOREIGN KEY (`ID_Pegawai`) REFERENCES `mst_pegawai` (`ID_Pegawai`);

--
-- Ketidakleluasaan untuk tabel `resep_obat`
--
ALTER TABLE `resep_obat`
  ADD CONSTRAINT `resep_obat_ibfk_1` FOREIGN KEY (`ID_Pemeriksaan`) REFERENCES `pemeriksaan_medis` (`ID_Pemeriksaan`),
  ADD CONSTRAINT `resep_obat_ibfk_2` FOREIGN KEY (`ID_Obat`) REFERENCES `mst_obat` (`ID_Obat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
